#!/usr/bin/env bash
echo "classifier,dataset,frequency,iteration,step_size,reg_param,mini_batch_fraction,accuracy" > stanford-binary-custom.csv

#iters changing
sbt "run -c spark -d stanford -f 2 -i 10 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 30 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 100 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 300 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 3000 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 10000 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 30000 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"

#step changing
sbt "run -c spark -d stanford -f 2 -i 1000 -s 0.03 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 0.1 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 0.3 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 0.5 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 3.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 5.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 10.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 30.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"

#reg_param changing
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.03 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.3 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.5 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 1.0 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 3.0 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 5.0 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 10.0 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 30.0 -m 0.5 -o stanford-binary-custom.csv"

#batch_size changing
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.1 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.2 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.3 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.4 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.5 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.6 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.7 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.8 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.9 -o stanford-binary-custom.csv"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 1.0 -o stanford-binary-custom.csv"