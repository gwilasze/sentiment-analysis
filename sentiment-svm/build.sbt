name := "svm-sentiment"

scalaVersion := "2.10.4"

version := "1.0"

resolvers += "Jage repositories" at "http://age.iisg.agh.edu.pl/maven2"

libraryDependencies ++= Seq(
  "org.scalanlp" % "nak" % "1.2.1",
  "org.apache.spark" %% "spark-core" % "1.1.0",
  "org.apache.spark" %% "spark-mllib" % "1.1.0",
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "log4j" % "apache-log4j-extras" % "1.2.17",
  "org.rogach" %% "scallop" % "0.9.5",
  "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2",
  "org.jage.services" % "services" %"2.7.0-SNAPSHOT",
  "org.jage.services" % "core" %"2.7.0-SNAPSHOT"
)

scalacOptions ++= Seq("-deprecation", "-feature")

fork in run := true

javaOptions in run ++= Seq("-Xms1G", "-Xmx5G")
