#!/usr/bin/env bash
echo "classifier,dataset,frequency,iteration,step_size,reg_param,mini_batch_fraction,accuracy" > tweets-binary2.csv

sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.0 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.005 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.01 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.03 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.05 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.1 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.3 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 0.5 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 1.0 -m 0.5 -o tweets-binary2.csv --directory tweets"
sbt "run -c spark -d stanford -f 2 -i 1000 -s 1.0 -r 10.0 -m 0.5 -o tweets-binary2.csv --directory tweets"