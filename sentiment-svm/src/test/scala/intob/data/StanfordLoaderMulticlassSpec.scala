package intob.data

import com.google.common.io.Resources
import intob.classifier.Classifier._
import intob.data.Util._
import org.scalatest.WordSpec

class StanfordLoaderMulticlassSpec extends WordSpec {
  val stanfordDatasetDirectory = Resources.getResource("stanford_dataset_multiclass").getPath

  "StanfordLoaderMulticlass" should {

    "prepare vector of feature words" in {
      // when
      val loader = new StanfordLoaderMulticlass(stanfordDatasetDirectory)

      // then
      // order checked manually
      val expectedWords = Vector("second", "review", "train", "positive", "negative", "first")

      assertResult(expectedWords)(loader.featureWords)
    }

    "return train data" in {
      // given
      val loader = new StanfordLoaderMulticlass(stanfordDatasetDirectory)

      // when
      val result: LabelledData = loader.trainData

      // then
      // Vector("second", "review", "train", "negative", "positive", "first") - if word is present in file then 1.0
      // else 0.0
      val expectedDataWithFeatures = Vector(
        (Vector(0.0, 1.0, 1.0, 1.0, 0.0, 1.0), 0),
        (Vector(1.0, 1.0, 1.0, 1.0, 0.0, 0.0), 0),
        (Vector(1.0, 1.0, 1.0, 0.0, 1.0, 0.0), 2),
        (Vector(0.0, 1.0, 1.0, 0.0, 1.0, 1.0), 4)
      )

      assertResult(expectedDataWithFeatures)(toScalaVectors(result))
    }

    "return test data" in {
      // given
      val loader = new StanfordLoaderMulticlass(stanfordDatasetDirectory)

      // when
      val result: LabelledData = loader.testData

      // then
      // Vector("second", "review", "train", "negative", "positive", "first") - if word is present in file then 1.0
      // else 0.0
      val expectedDataWithFeatures = Vector(
        (Vector(0.0, 0.0, 0.0, 1.0, 0.0, 1.0), 1),
        (Vector(1.0, 0.0, 0.0, 1.0, 0.0, 0.0), 2),
        (Vector(0.0, 0.0, 0.0, 0.0, 1.0, 1.0), 3),
        (Vector(1.0, 0.0, 0.0, 0.0, 1.0, 0.0), 3)
      )

      assertResult(expectedDataWithFeatures)(toScalaVectors(result))
    }
  }
}