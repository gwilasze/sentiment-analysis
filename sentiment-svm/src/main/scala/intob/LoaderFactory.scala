package intob

import com.typesafe.scalalogging.slf4j.StrictLogging
import intob.data._

class LoaderFactory extends StrictLogging {
  def create(parsedOptions: ParseConfiguration): Loader = {
    logger.info(s"Creating loader for ${parsedOptions.dataset()}")
    val wordFrequency = if (parsedOptions.frequency.isDefined) {
      parsedOptions.frequency()
    } else {
      1
    }

    parsedOptions.dataset() match {
      case "large-movie-dataset" => {
        logger.info("creating LargeMovieDatasetLoader")

        if (parsedOptions.directory.isDefined) {
          new LargeMovieDatasetLoader(parsedOptions.directory(), wordFrequency)
        } else {
          new LargeMovieDatasetLoader(requiredWordFrequency = wordFrequency)
        }
      }
      case "stanford" => {
        logger.info("creating StanfordLoaderBinary")

        if (parsedOptions.directory.isDefined) {
          new StanfordLoaderBinary(parsedOptions.directory(), wordFrequency)
        } else {
          new StanfordLoaderBinary(requiredWordFrequency = wordFrequency)
        }
      }
      case "stanford-multiclass" => {
        logger.info("creating StanfordLoaderMulticlass")

        if (parsedOptions.directory.isDefined) {
          new StanfordLoaderMulticlass(parsedOptions.directory(), wordFrequency)
        } else {
          new StanfordLoaderMulticlass(requiredWordFrequency = wordFrequency)
        }
      }
      case "har" | "iris" => {
        logger.info("creating HARFormatDatasetLoader")

        if (parsedOptions.directory.isDefined) {
          new HARFormatDatasetLoader(parsedOptions.directory())
        } else {
          new HARFormatDatasetLoader
        }
      }
    }
  }

}