package intob.data

import java.io.File

import intob.classifier.Classifier
import org.apache.commons.io.FileUtils

import scala.collection.JavaConverters._
import scala.io.Source

/**
 * Loads so called "large-move-dataset": http://ai.stanford.edu/~amaas/data/sentiment/
 *
 * @param largeMovieDatasetPath path to base directory
 * @param requiredWordFrequency minimum word frequency to be included in feature words vector
 */
class LargeMovieDatasetLoader(largeMovieDatasetPath: String = "large-movie-dataset",
                              requiredWordFrequency: Int = 1) extends SentimentReviewLoader {

  private val positiveTrainDirectory =
    new File(largeMovieDatasetPath + File.separator + "train" + File.separator + "pos")
  private val negativeTrainDirectory =
    new File(largeMovieDatasetPath + File.separator + "train" + File.separator + "neg")
  private val positiveTestDirectory = new File(largeMovieDatasetPath + File.separator + "test" + File.separator + "pos")
  private val negativeTestDirectory = new File(largeMovieDatasetPath + File.separator + "test" + File.separator + "neg")
  private val trainReviews = loadReviews(positiveTrainDirectory, negativeTrainDirectory)
  private val _featureWords: Vector[String] = prepareFeatureWords(trainReviews, requiredWordFrequency)

  private def loadReviews(positiveDirectory: File, negativeDirectory: File): ReviewsData = {
    logger.info(s"Loading reviews from ${positiveDirectory.getAbsolutePath} and ${negativeDirectory.getAbsolutePath}")

    val positiveFiles = FileUtils.listFiles(positiveDirectory, Array[String]("txt"), false).asScala.toVector
    logger.info("Fetched positive files")
    val negativeFiles = FileUtils.listFiles(negativeDirectory, Array[String]("txt"), false).asScala.toVector
    logger.info("Fetched negative files")

    val positiveReviews = positiveFiles.map(processFileReturnReviews(_))
    logger.info("Created positive reviews")
    val negativeReviews = negativeFiles.map(processFileReturnReviews(_))
    logger.info("Created negative reviews")

    val resultMap = Map(0 -> negativeReviews, 1 -> positiveReviews)
    ReviewsData(resultMap)
  }

  private def processFileReturnReviews(file: File): Vector[String] = {
    logger.debug(s"Processing review file for words: ${file.getName}")

    val bufferedSource = Source.fromFile(file)
    val lines = bufferedSource.getLines()

    val words = lines.flatMap(reviewToWords)
    val result = words.toVector
    bufferedSource.close()

    result
  }

  override def featureWords: Vector[String] = _featureWords

  /*
  Return data with 0 or 1 labels
   */
  override def trainData: Classifier.LabelledData = {
    val result = prepareLabelledData(trainReviews)
    logger.info("prepared train data for classifier")

    result
  }

  /*
  Return data with 0 or 1 labels
   */
  override def testData: Classifier.LabelledData = {
    logger.info("starting to load test data")
    val testReviews = loadReviews(positiveTestDirectory, negativeTestDirectory)
    logger.info("loaded to load test data")

    val result = prepareLabelledData(testReviews)
    logger.info("prepared test data for classifier")

    result
  }
}