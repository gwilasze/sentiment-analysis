package intob.data

import intob.classifier.{Classifier, IntegerLabeledPoint}
import org.apache.spark.mllib.linalg.Vectors

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
 * Trait for all loaders that load review data, contains useful methods that will help in implementing such loader
 */
trait SentimentReviewLoader extends Loader {

  /**
   *
   * @param review Single review, may contain whitespaces, new lines etc. It will be splitted, only words
   *               that are longer than 2 letters will be kept.
   * @return Vector of words,
   */
  protected def reviewToWords(review: String): Vector[String] = {
    val allWords = review.split(whitespaceRegex)
    val longWords = allWords.filter(_.size > 2)
    val lowercaseWords = longWords.map(_.toLowerCase)

    lowercaseWords.toVector
  }

  /**
   *
   * @param reviews Reviews from which feature words will be extracted, usually those are the train reviews.
   * @param requiredWordFrequency feature words vector will only contain words that were encountered at least
   *                              requiredWordFrequency times
   * @return feature words with given frequency
   */
  protected def prepareFeatureWords(reviews: ReviewsData, requiredWordFrequency: Int): Vector[String] = {
    logger.info("Preparing feature words")

    val wordsMap = new mutable.HashMap[String, Int]()

    reviews.labelsToReviews.foreach { tuple =>
      val label = tuple._1
      val reviews = tuple._2

      reviews.foreach(addWords(wordsMap, _))
      logger.info(s"Processed reviews with label $label and added words to feature words")
    }

    val frequentWords = wordsMap.filter(keyAndValue => keyAndValue._2 >= requiredWordFrequency)
    val result = frequentWords.keySet.toVector

    logger.info(s"Loaded ${result.size} feature words with frequency >= $requiredWordFrequency")

    result
  }

  private def addWords(wordsSet: mutable.HashMap[String, Int], words: Vector[String]): Unit = {

    words.foreach { word =>
      if (wordsSet.contains(word) == false) {
        wordsSet.put(word, 0)
      }

      val count = wordsSet.get(word).get

      wordsSet.update(word, count + 1)
    }
  }

  /**
   * Creates feature vector and label for classifiers based on bag of words technique.
   *
   * @param featureWords extracted feature words
   * @param words splitted review
   * @param label the label of the result
   * @return single data point for classifier
   */
  protected def reviewToFeatureAndLabelVector(featureWords: Vector[String], words: Vector[String], label: Int):
  Classifier.LabelledDataInstance = {
    val numberOfFeatures = featureWords.size
    val featuresBuffer = new ArrayBuffer[Double]()
    val indexes = new ArrayBuffer[Int]()

    for (featureIndex <- 0 until numberOfFeatures) {
      val featureWord = featureWords(featureIndex)

      if (words.contains(featureWord)) {
        featuresBuffer.append(1.0)
        indexes.append(featureIndex)
      }
    }

    IntegerLabeledPoint(label, Vectors.sparse(numberOfFeatures, indexes.toArray, featuresBuffer.toArray))
  }

  /**
   *
   * @param reviews reviews based on which feature vectors with labels will be created
   * @return Array of labels with feature vectors that are ready to pass them to classifier
   */
  protected def prepareLabelledData(reviews: ReviewsData): Classifier.LabelledData = {
    logger.info("Preparing labelled data for classifier")
    val dataWithFeatures = ArrayBuffer[Classifier.LabelledDataInstance]()

    reviews.labelsToReviews.foreach { tuple =>
      val label = tuple._1
      val reviews = tuple._2
      logger.info(s"starting to process reviews with label $label for labelled data")

      val sentimentData = reviews.toSeq.map(words => reviewToFeatureAndLabelVector(featureWords, words, label))
      dataWithFeatures.appendAll(sentimentData)

      logger.info(s"processed reviews with label $label for labelled data")
    }

    dataWithFeatures.toArray
  }

  /**
   *
   * @return all extracted feature words
   */
  def featureWords: Vector[String]
}