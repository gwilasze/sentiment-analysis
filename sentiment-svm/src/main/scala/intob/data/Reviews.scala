package intob.data

object ReviewsData {
  type SplitReview = Vector[String]
  type ReviewsFromFiles = Iterable[SplitReview]
}

/**
 * Holds information about some reviews (for example test/train)
 * @param labelsToReviews mapping from label of the review to splitted review
 */
case class ReviewsData(labelsToReviews: Map[Int, ReviewsData.ReviewsFromFiles])