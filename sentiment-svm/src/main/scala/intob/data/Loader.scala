package intob.data

import com.typesafe.scalalogging.slf4j.StrictLogging
import intob.classifier.Classifier

/**
 * Base trait for all types of data loaders
 *
 * If the loader is used for binary classified data it must return labels as {0,1}, multiclass loaders can return
 * labels as any integer values as they want.
 */
trait Loader extends StrictLogging {

  protected val whitespaceRegex = "\\s"
  protected val whitespacesRegex = "\\s+"

  /**
   *
   * @return Data that will be used to train some classifier, if the data contains only two labels those labels must
   *         be returned as {0, 1},
   */
  def trainData: Classifier.LabelledData

  /**
   *
   * @return Data that will be used to test some classifier, if the data contains only two labels those labels must
   *         be returned as {0, 1}.
   */
  def testData: Classifier.LabelledData
}