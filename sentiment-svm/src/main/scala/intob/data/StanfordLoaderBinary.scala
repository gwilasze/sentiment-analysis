package intob.data

import java.io.File

import intob.classifier.Classifier.LabelledData

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
 * Loads binary version of stanford dataset.
 *
 * Unprocessed trees can be found here: http://nlp.stanford.edu/sentiment/trainDevTestTrees_PTB.zip
 *
 * Dataset should contain at least two files: train.txt, test.txt. Each line should be in format
 * <review>|[positive|negative]
 *
 * @param datasetDirectory path to directory with test.txt and train.txt files
 * @param requiredWordFrequency minimum word frequency to be included in feature words vector
 */
class StanfordLoaderBinary(datasetDirectory: String = "src/main/resources/stanford_dataset_binary",
                     requiredWordFrequency: Int = 1) extends SentimentReviewLoader {

  private val trainReviewsFileName = new File(datasetDirectory + File.separator + "train.txt")
  private val testReviewsFileName = new File(datasetDirectory + File.separator + "test.txt")
  private val trainReviews: ReviewsData = loadReviews(trainReviewsFileName)
  private val _featureWords: Vector[String] = prepareFeatureWords(trainReviews, requiredWordFrequency)

  private def loadReviews(file: File): ReviewsData = {
    logger.info(s"Loading reviews from ${file.getAbsolutePath}")

    val reviewsBufferedSource = Source.fromFile(file)
    val reviews = reviewsBufferedSource.getLines()
    val positiveReviews = ArrayBuffer[Vector[String]]()
    val negativeReviews = ArrayBuffer[Vector[String]]()

    reviews.foreach(line => processReviewAddToList(positiveReviews, negativeReviews, line))

    logger.info(s"Loaded ${positiveReviews.size} positive reviews and ${negativeReviews.size} negative reviews")

    reviewsBufferedSource.close()

    val resultMap = Map(0 -> negativeReviews.toVector, 1 -> positiveReviews.toVector)
    ReviewsData(resultMap)
  }

  private def processReviewAddToList(positiveReviews: ArrayBuffer[Vector[String]],
                                     negativeReviews: ArrayBuffer[Vector[String]],
                                     reviewWithLabel: String): Unit = {
    val splitLine = reviewWithLabel.split("\\|")
    val words = reviewToWords(splitLine(0))
    val label = toLabel(splitLine(1))

    if (label == 1) {
      positiveReviews.append(words)
    } else {
      negativeReviews.append(words)
    }
  }

  private def toLabel(label: String): Int = label match {
    case "positive" => 1
    case "negative" => 0
  }

  override def featureWords: Vector[String] = _featureWords

  /**
   *
   * @return Data that will be used to train some classifier, all labels will have value 0 or 1. Based on train.txt.
   */
  override def trainData: LabelledData = {
    val result = prepareLabelledData(trainReviews)
    logger.info("prepared train data for classifier")

    result
  }

  /**
   *
   * @return Data that will be used to test some classifier, all labels will have value 0 or 1. Based on test.txt
   */
  override def testData: LabelledData = {
    logger.info("starting to load test data")
    val testReviews = loadReviews(testReviewsFileName)
    logger.info("loaded to load test data")

    val result = prepareLabelledData(testReviews)
    logger.info("prepared test data for classifier")

    result
  }
}