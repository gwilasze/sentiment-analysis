package intob.classifier

object Kernels {

  import intob.classifier.Classifier.DataType

  type Kernel = (DataType, DataType) => Double

  def linear: Kernel = new Kernel {
    override def apply(v1: DataType, v2: DataType): Double = {
      var sum = 0.0

      for (idx <- 0 until v1.size) {
        sum = sum + v1(idx) * v2(idx)
      }

      sum
    }
  }

  def rbf(sigma: Double): Kernel = new Kernel {
    override def apply(v1: DataType, v2: DataType): Double = {
      def blah(x: Double, y: Double) = ( x - y ) * ( x + y )
      var s = 0.0

      for (idx <- 0 until v1.size) {
        s = s + blah(v1(idx), v2(idx))
      }

      Math.exp(-1.0 * s / ( 2.0 * sigma * sigma ))
    }
  }
}