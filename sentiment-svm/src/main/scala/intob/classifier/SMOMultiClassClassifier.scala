package intob.classifier

import intob.classifier.Classifier.DataType

/**
 * Classifier based on custom implemented SMO technique. This is multiclass classifier using one-vs-all technique
 * @param options configuration options for classifier
 */
class SMOMultiClassClassifier(val options: SMOClassifierOptions) extends Classifier {

  import intob.classifier.Classifier.LabelledData

  private var oneVsAllClassifiers: Map[Int, SMOClassifier] = Map.empty
  private var labels: Set[Int] = Set.empty

  private def toSingleLabel(instance: IntegerLabeledPoint, onlyLabel: Int): IntegerLabeledPoint = {
    if (instance.label == onlyLabel) {
      instance.copy(label = 1)
    } else {
      instance.copy(label = -1)
    }
  }

  /**
   *
   * @param labeledData can contain arbitrarily labelled data
   */
  override def train(labeledData: LabelledData): Unit = {
    labels = labeledData.map(x => x.label).toSet

    for (label <- labels) {
      val oneVsAllData = labeledData.map(toSingleLabel(_, label))
      val oneVsAllClassifier = new SMOClassifier(options)
      oneVsAllClassifier.train(oneVsAllData)
      oneVsAllClassifiers = oneVsAllClassifiers.updated(label, oneVsAllClassifier)
    }
  }

  /**
   *
   * @param inst feature vector to classify
   * @return one of the labels that were encountered during training
   */
  override def predict(inst: DataType): Int = {
    labels.map(label => (label, oneVsAllClassifiers(label).marginOne(inst))).maxBy(x => x._2)._1
  }
}