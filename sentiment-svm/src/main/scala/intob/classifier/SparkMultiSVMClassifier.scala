package intob.classifier

import intob.classifier.Classifier.{DataType, LabelledData}

/**
 * Classifier that uses Spark SVM implementation. This is multiclass classifier using one-vs-all technique
 * @param options configuration options for classifier
 */
class SparkMultiSVMClassifier(val options: SparkSVMOptions) extends Classifier {

  private var oneVsAllClassifiers: Map[Int, SparkSVMClassifier] = Map.empty
  private var labels: Set[Int] = Set.empty

  private def toSingleLabel(instance: IntegerLabeledPoint, onlyLabel: Int): IntegerLabeledPoint = {
    if (instance.label == onlyLabel) {
      instance.copy(label = 1)
    } else {
      instance.copy(label = 0)
    }
  }

  /**
   *
   * @param labelledData can contain arbitrarily labelled data
   */
  override def train(labelledData: LabelledData): Unit = {
    labels = labelledData.map(_.label).toSet

    for (label <- labels) {
      val oneVsAllData = labelledData.map(toSingleLabel(_, label))
      val oneVsAllClassifier = new SparkSVMClassifier(options)
      oneVsAllClassifier.train(oneVsAllData)
      oneVsAllClassifier.model.clearThreshold()
      oneVsAllClassifiers = oneVsAllClassifiers.updated(label, oneVsAllClassifier)
    }
  }

  /**
   *
   * @param inst feature vector to classify
   * @return one of the labels that were encountered during training
   */
  override def predict(inst: DataType): Int = {
    labels.map(label => (label, oneVsAllClassifiers(label).margin(inst))).maxBy(x => x._2)._1
  }
}