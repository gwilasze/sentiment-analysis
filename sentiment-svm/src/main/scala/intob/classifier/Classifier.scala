package intob.classifier

import org.apache.spark.mllib.linalg.Vector
import org.jage.strategy.IStrategy

object Classifier {
  type DataType = Vector
  type LabelledDataInstance = IntegerLabeledPoint
  type LabelledData = Array[LabelledDataInstance]
}

trait Classifier extends IStrategy {

  import intob.classifier.Classifier._

  def train(labeledData: LabelledData): Unit

  def predict(inst: Classifier.DataType): Int

  def options: ClassifierOptions
}








