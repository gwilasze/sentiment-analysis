package intob.classifier

import com.typesafe.scalalogging.slf4j.StrictLogging

import scala.util.Random

/**
 * Classifier based on custom implemented SMO technique
 * @param options configuration options for classifier
 */
class SMOClassifier(val options: SMOClassifierOptions) extends Classifier with StrictLogging {

  import intob.classifier.Classifier.{DataType, LabelledData}

  var bias = 0.0
  var alpha: Array[Double] = Array()
  var labelledData: LabelledData = new Array[Classifier.LabelledDataInstance](0)

  private def oneOrMinusOneLabel(instance: Classifier.LabelledDataInstance): Classifier.LabelledDataInstance = {
    if (instance.label == 0) {
      instance.copy(label = -1)
    } else {
      instance
    }
  }

  /**
   *
   * @param inst feature vector to classify
   * @return one of {0, 1}
   */
  override def predict(inst: DataType): Int = {
    if (marginOne(inst) > 0) {
      1
    } else {
      0
    }
  }

  /**
   *
   * @param labelledData should contain {0, 1} labeled data
   */
  override def train(labelledData: Classifier.LabelledData) = {
    this.labelledData = labelledData.map(oneOrMinusOneLabel)
    val examples = labelledData.length

    smo(examples)

    // okay, we need to retain all the support vectors in the training data,
    // we can't just get away with computing the weights and throwing it out

    // But! We only need to store the support vectors for evaluation of testing
    // instances. So filter here based on this.alpha[i]. The training data
    // for which this.alpha[i] = 0 is irrelevant for future.

    val res: (IndexedSeq[Double], IndexedSeq[Classifier.LabelledDataInstance]) = alpha.zip(this.labelledData).filter(
      x => {
        val alphaI = x._1
        alphaI > options.alphaTolerance
      }).unzip

    alpha = res._1.toArray
    this.labelledData = res._2.toArray
  }

  private def smo(examples: Int) = {
    val (data, labels) = labelledData.map(data => (data.features, data.label)).unzip

    var iter = 0
    var passes = 0
    alpha = Array.fill(examples)(0)

    bias = 0.0
    while (passes < options.noChangeDataMaxPasses && iter < options.maxIterations) {
      logger info ( "iteration: " + iter )

      var alphaChanged = 0

      var i = 0
      while (i < examples) {
        //logger info ("i: " + i)

        val Ei = marginOne(data(i)) - labels(i)

        if (( labels(i) * Ei < -options.tolerance && alpha(i) < options.C )
          || ( labels(i) * Ei > options.tolerance && alpha(i) > 0 )) {

          // alphaI needs updating! Pick a j to update it with

          val randomGenerator = new Random()

          var j = i
          while (j == i) j = randomGenerator.nextInt(examples)

          val Ej = marginOne(data(j)) - labels(j)

          // calculate L and H bounds for j to ensure we're in [0 C]x[0 C] box
          val ai = alpha(i)
          val aj = alpha(j)
          var L = 0.0
          var H = options.C
          if (labels(i) == labels(j)) {
            L = Math.max(0.0, ai + aj - options.C)
            H = Math.min(options.C, ai + aj)
          } else {
            L = Math.max(0.0, aj - ai)
            H = Math.min(options.C, options.C + aj - ai)
          }

          var continue = false

          if (Math.abs(L - H) < 1e-4) continue = true

          var eta = 0.0
          if (!continue) {
            eta = ( 2 * options.kernel(data(i), data(j))
              - options.kernel(data(i), data(i))
              - options.kernel(data(j), data(j)) )

            if (eta >= 0) continue = true
          }

          var newaj = 0.0
          if (!continue) {
            // compute new alpha_j and clip it inside [0 C]x[0 C] box
            // then compute alphaI based on it.
            newaj = aj - labels(j) * ( Ei - Ej ) / eta
            if (newaj > H) newaj = H
            if (newaj < L) newaj = L

            if (Math.abs(aj - newaj) < 1e-4) continue = true
          }

          if (!continue) {
            alpha = alpha.updated(j, newaj)
            val newai = ai + labels(i) * labels(j) * ( aj - newaj )
            alpha = alpha.updated(i, newai)

            // update the bias term
            val b1 = ( bias - Ei - labels(i) * ( newai - ai ) * options.kernel(data(i), data(i))
              - labels(j) * ( newaj - aj ) * options.kernel(data(i), data(j)) )
            val b2 = ( bias - Ej - labels(i) * ( newai - ai ) * options.kernel(data(i), data(j))
              - labels(j) * ( newaj - aj ) * options.kernel(data(j), data(j)) )
            bias = 0.5 * ( b1 + b2 )
            if (newai > 0 && newai < options.C) bias = b1
            if (newaj > 0 && newaj < options.C) bias = b2

            alphaChanged += 1
          }

        }
        i += 1
      }

      iter += 1
      if (alphaChanged == 0) passes += 1
      else passes = 0

    }
  }

  // Returns margin of given example
  // this is the core prediction function. All others are for convenience mostly
  // and end up calling this one somehow.
  private[classifier] def marginOne(inst: DataType): Double = {
    def blah(alphaI: Double, dataRow: IntegerLabeledPoint): Double = {
      alphaI * dataRow.label * options.kernel(inst, dataRow.features)
    }

    val valueForInst = alpha.zip(labelledData).map(x => blah(x._1, x._2)).sum

    valueForInst + bias
  }
}