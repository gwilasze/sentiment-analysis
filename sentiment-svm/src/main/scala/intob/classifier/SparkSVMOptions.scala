package intob.classifier

/**
 * Options for Spark SVM classifier, defaults taken from Spark source code:
 * https://github.com/apache/spark/blob/v1.1.0/mllib/src/main/scala/org/apache/spark/mllib/classification/SVM.scala
 * @param iterations Number of iterations of gradient descent to run.
 * @param stepSize Step size to be used for each iteration of gradient descent.
 * @param regParam Regularization parameter.
 * @param miniBatchFraction Fraction of data to be used per iteration.
 */
case class SparkSVMOptions(iterations: Int = 100,
                           stepSize: Double = 1.0,
                           regParam: Double = 1.0,
                           miniBatchFraction: Double = 1.0) extends ClassifierOptions