package intob

case class PredictResult(predictedLabel: Int, expectedLabel: Int)