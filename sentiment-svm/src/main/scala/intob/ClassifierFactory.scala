package intob

import com.typesafe.scalalogging.slf4j.StrictLogging
import intob.classifier._

class ClassifierFactory extends StrictLogging {
  def create(parsedOptions: ParseConfiguration): Classifier = {
    logger.info(s"Creating ${parsedOptions.classifier()} classifier")
    val smoConfiguration = createSMOConfiguration(parsedOptions)
    val sparkSvmConfiguration = createSparkSVMOptions(parsedOptions)

    parsedOptions.classifier() match {
      case "spark" => new SparkSVMClassifier(sparkSvmConfiguration)
      case "spark-multiclass" => new SparkMultiSVMClassifier(sparkSvmConfiguration)
      case "smo" => new SMOClassifier(smoConfiguration)
      case "smo-multiclass" => new SMOMultiClassClassifier(smoConfiguration)
    }
  }

  private def createSMOConfiguration(parsedOptions: ParseConfiguration) = {
    var options = SMOClassifierOptions()

    if (parsedOptions.iterations.isDefined) {
      options = options.copy(maxIterations = parsedOptions.iterations())
    }

    if (parsedOptions.regParam.isDefined) {
      options = options.copy(C = parsedOptions.regParam())
    }

    logger.info(s"Created $options")

    options
  }

  private def createSparkSVMOptions(parsedOptions: ParseConfiguration) = {
    var options = SparkSVMOptions()

    if (parsedOptions.iterations.isDefined) {
      options = options.copy(iterations = parsedOptions.iterations())
    }

    if (parsedOptions.stepSize.isDefined) {
      options = options.copy(stepSize = parsedOptions.stepSize())
    }

    if (parsedOptions.regParam.isDefined) {
      options = options.copy(regParam = parsedOptions.regParam())
    }

    if (parsedOptions.miniBatchFraction.isDefined) {
      options = options.copy(miniBatchFraction = parsedOptions.miniBatchFraction())
    }

    logger.info(s"Created $options")

    options
  }
}