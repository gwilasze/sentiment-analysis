import re
import sys
import os

POSITIVE = 'positive'
NEGATIVE = 'negative'

binary = True

binary_sentiments_map = {
    "0": NEGATIVE,
    "1": NEGATIVE,
    "2": None,
    "3": POSITIVE,
    "4": POSITIVE
}

def prepare(sentence):
    if binary:
        sentiment = binary_sentiments_map[sentence[0]]
        if not sentiment:
            return ""
    else:
        sentiment = sentence[0]

    return '{0}|{1}'.format(sentence[2:].strip(), sentiment)


def read_lines(file_name):
    with open(file_name, 'r') as file:
        return file.readlines()


def process_file(file_name):
    with_sentiment = [prepare(line) for line in read_lines(file_name)]
    return [line for line in with_sentiment if line]


def save_to_file(file_name, lines):
    with open(file_name, 'w+') as output_file:
        for line in lines:
            output_file.write(line + os.linesep)




if __name__ == '__main__':
    input_file_name = sys.argv[1]
    output_file_name = sys.argv[2]

    sentences = process_file(input_file_name)
    save_to_file(output_file_name, sentences)
