# based on https://github.com/rafacarrascosa/samr/blob/develop/samr/inquirer_lex_transform.py

from collections import namedtuple, defaultdict
import csv

FIELDS = ("Entry, Source, Positiv, Negativ, Pstv, Affil, Ngtv, Hostile, Strong,"
          " Power, Weak, Submit, Active, Passive, Pleasur, Pain, Feel, Arousal,"
          " EMOT, Virtue, Vice, Ovrst, Undrst, Academ, Doctrin, Econ, Exch, "
          "ECON, Exprsv, Legal, Milit, Polit, POLIT, Relig, Role, COLL, Work, "
          "Ritual, SocRel, Race, Kin, MALE, Female, Nonadlt, HU, ANI, PLACE, "
          "Social, Region, Route, Aquatic, Land, Sky, Object, Tool, Food, "
          "Vehicle, BldgPt, ComnObj, NatObj, BodyPt, ComForm, COM, Say, Need, "
          "Goal, Try, Means, Persist, Complet, Fail, NatrPro, Begin, Vary, "
          "Increas, Decreas, Finish, Stay, Rise, Exert, Fetch, Travel, Fall, "
          "Think, Know, Causal, Ought, Perceiv, Compare, Eval, EVAL, Solve, "
          "Abs, ABS, Quality, Quan, NUMB, ORD, CARD, FREQ, DIST, Time, TIME, "
          "Space, POS, DIM, Rel, COLOR, Self, Our, You, Name, Yes, No, Negate, "
          "Intrj, IAV, DAV, SV, IPadj, IndAdj, PowGain, PowLoss, PowEnds, "
          "PowAren, PowCon, PowCoop, PowAuPt, PowPt, PowDoct, PowAuth, PowOth, "
          "PowTot, RcEthic, RcRelig, RcGain, RcLoss, RcEnds, RcTot, RspGain, "
          "RspLoss, RspOth, RspTot, AffGain, AffLoss, AffPt, AffOth, AffTot, "
          "WltPt, WltTran, WltOth, WltTot, WlbGain, WlbLoss, WlbPhys, WlbPsyc, "
          "WlbPt, WlbTot, EnlGain, EnlLoss, EnlEnds, EnlPt, EnlOth, EnlTot, "
          "SklAsth, SklPt, SklOth, SklTot, TrnGain, TrnLoss, TranLw, MeansLw, "
          "EndsLw, ArenaLw, PtLw, Nation, Anomie, NegAff, PosAff, SureLw, If, "
          "NotLw, TimeSpc, FormLw, Othtags, Defined")

InquirerLexEntry = namedtuple("InquirerLexEntry", FIELDS)
FIELDS = InquirerLexEntry._fields


def load_dictionary(filename):
  corpus = defaultdict(list)
  it = csv.reader(open(filename), delimiter="\t")
  next(it)  # Drop header row
  for row in it:
      entry = InquirerLexEntry(*row)
      name = entry.Entry.lower()
      if "#" in name:
          name = name[:name.index("#")]

      features = []

      for i in xrange(1, len(FIELDS)):
        feature, exist = FIELDS[i], entry[i]
        if exist:
          features.append(feature)

      corpus[name].extend(features)

  return corpus

def extract_column(column_name, destination_filename, dictionary):
  words = [entry for (entry, features) in dictionary.items() if column_name in features]

  with open(destination_filename, 'w') as output_file:
    for word in words:
      output_file.write(word + '\n')


dictionary = load_dictionary('inquirer-dictionary.txt')
extract_column('Positiv', 'positive.txt', dictionary)
extract_column('Negativ', 'negative.txt', dictionary)
extract_column('NotLw', 'inv.txt', dictionary)
extract_column('SureLw', 'inc.txt', dictionary)
extract_column('If', 'dec.txt', dictionary)