Installation:

1. install additional data stuff for nltk library. 
  - start python interpreter
  - type `import nltk`
  - type `nltk.download()`
  - download corpora/wordnet, maxent_treebank_pos_tagger, punkt_tokenizer modules using opened window

2. run ./download_dict.sh
3. run python create_dict.py
4. everything is ready now for sentiment classification.

Results:

[INFO] positive examples tested
('true positive: ', 550, 'missed positive: ', 359)
[INFO] negative examples tested
('true negative: ', 305, 'missed negative: ', 607)
