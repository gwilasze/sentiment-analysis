import re

import learn


POSITIVE = 'positive'
NEGATIVE = 'negative'

sentiments_map = {
    "0": NEGATIVE,
    "1": NEGATIVE,
    "2": None,
    "3": POSITIVE,
    "4": POSITIVE
}


def prepare(sentence):
    sentiment = sentiments_map[sentence[1]]
    return ' '.join([word for word in re.split('\W+|[0-4]', sentence) if word != ""]), sentiment


def read_lines(file_name):
    with open(file_name, 'r') as file:
        return file.readlines()


def prepare_data(file_name):
    return [sentence for sentence in [prepare(line) for line in read_lines(file_name)] if sentence is not None]


def test_sentiment(sentiments, classifier):
    true_result = 0
    false_result = 0
    for sentence, sentiment in sentiments:
        if classifier.sentiment_of(sentence) == sentiment:
            true_result += 1
        else:
            false_result += 1

    return true_result, false_result

if __name__ == '__main__':
    test_file = '../sentiment-nltk/test.txt'

    test_data = prepare_data(test_file)
    pos_test = [t for t in test_data if t[1] == POSITIVE]

    classifier = learn.DictionarySentimentClassifier()

    true_positive, false_positive = test_sentiment(pos_test, classifier)
    print('[INFO] positive examples tested')
    print('true positive: ', true_positive, 'missed positive: ', false_positive)

    neg_test = [t for t in test_data if t[1] == NEGATIVE]

    true_negative, false_negative = test_sentiment(neg_test, classifier)
    print('[INFO] negative examples tested')
    print('true negative: ', true_negative, 'missed negative: ', false_negative)

    true_all = true_negative + true_positive
    false_all = false_negative + false_positive

    accuracy = float(true_all) / (true_all + false_all)

    print('true all: ', true_all, ' false all: ', false_all, ' accuracy: ', accuracy)
