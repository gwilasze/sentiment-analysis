## based on http://fjavieralba.com/basic-sentiment-analysis-with-python.html

import nltk
from pprint import pprint
from collections import defaultdict
from nltk.stem import WordNetLemmatizer
from nltk.corpus.reader import wordnet

class Splitter(object):
    def __init__(self):
        self.nltk_splitter = nltk.data.load('tokenizers/punkt/english.pickle')
        self.nltk_tokenizer = nltk.tokenize.TreebankWordTokenizer()

    def split(self, text):
        sentences = self.nltk_splitter.tokenize(text)
        tokenized_sentences = [self.nltk_tokenizer.tokenize(sent) for sent in sentences]
        return tokenized_sentences

def get_wordnet_pos(treebank_tag):
    if treebank_tag.startswith('J'):
        return wordnet.ADJ
    if treebank_tag.startswith('V'):
        return wordnet.VERB
    if treebank_tag.startswith('N'):
        return wordnet.NOUN
    if treebank_tag.startswith('R'):
        return wordnet.ADV
    
    return wordnet.NOUN

class POSTagger(object):
    def __init__(self):
        self.wnl = WordNetLemmatizer()
        pass

    def pos_tag(self, sentences):
        pos = [nltk.pos_tag(sentence) for sentence in sentences]
        pos = [[(word, str(self.wnl.lemmatize(word.lower(), get_wordnet_pos(postag))), [postag]) for (word, postag) in sentence] for sentence in pos]
        return pos


def load_words(filename):
    with open(filename, 'r') as input_file:
        return [word.strip() for word in input_file.readlines()[0:]]


POSITIVE = 'positive'
NEGATIVE = 'negative'
INCREMENTER = 'incrementer'
DECREMENTER = 'decrementer'
INVERTER = 'inverter'


class DictionaryTagger(object):
    def __init__(self):
        positives = load_words('positive.txt')
        negatives = load_words('negative.txt')
        incrementers = load_words('inc.txt')
        decremeneters = load_words('dec.txt')
        inverters = load_words('inv.txt')

        self.dictionary = defaultdict(list)
        
        for word in positives:
            self.dictionary[word].append(POSITIVE)

        for word in negatives:
            self.dictionary[word].append(NEGATIVE)

        for word in incrementers:
            self.dictionary[word].append(INCREMENTER)

        for word in decremeneters:
            self.dictionary[word].append(DECREMENTER)

        for word in inverters:
            self.dictionary[word].append(INVERTER)

    def tag(self, postagged_sentences):
        return [self.tag_sentence(sentence) for sentence in postagged_sentences]

    def tag_sentence(self, sentence):
        def tag_word(word):
            expression_form, lemma, tagging = word

            if expression_form in self.dictionary:
                tagging.extend(self.dictionary[lemma])

            return (expression_form, lemma, tagging)
            
        return [tag_word(word) for word in sentence]

def value_of(sentiment):
    if sentiment == POSITIVE:
        return 1
    
    if sentiment == NEGATIVE:
        return -1
    
    return 0

def sentence_score(sentence_tokens, previous_token, acum_score):
    if not sentence_tokens:
        return acum_score

    for current_token in sentence_tokens:
        tags = current_token[2]
        token_score = sum([value_of(tag) for tag in tags])

        if previous_token is not None:
            previous_tags = previous_token[2]

            if INCREMENTER in previous_tags:
                token_score *= 2.0
            elif DECREMENTER in previous_tags:
                token_score /= 2.0
            elif INVERTER in previous_tags:
                token_score *= -1.0

        previous_token = current_token
        acum_score += token_score

    return acum_score

def sentiment_score(review):
    return sum([sentence_score(sentence, None, 0.0) for sentence in review])

POSITIVE_SENTIMENT = 'positive'
NEUTRAL_SENTIMENT = 'neutral'
NEGATIVE_SENTIMENT = 'negative'

def score_to_sentiment(score):
    if score > 0.0:
        return POSITIVE_SENTIMENT
    if score < 0.0:
        return NEGATIVE_SENTIMENT

    return NEGATIVE_SENTIMENT

class DictionarySentimentClassifier(object):
    def __init__(self):
        self.splitter = Splitter()
        self.postagger = POSTagger()
        self.dicttagger = DictionaryTagger()

    def sentiment_of(self, text):
        splitted_sentences = self.splitter.split(text)
        pos_tagged_sentences = self.postagger.pos_tag(splitted_sentences)
        dict_tagged_sentences = self.dicttagger.tag(pos_tagged_sentences)
        #pprint(dict_tagged_sentences)

        score = sentiment_score(dict_tagged_sentences)
        return score_to_sentiment(score)


def demo():
    pos_tweets = [('I love this car', 'positive'),
                  ('This view is amazing', 'positive'),
                  ('I feel great this morning', 'positive'),
                  ('I am so excited about the concert', 'positive'),
                  ('He is my best friend', 'positive')]

    neg_tweets = [('I do not like this car', 'negative'),
                  ('This view is horrible', 'negative'),
                  ('I feel tired this morning', 'negative'),
                  ('I am not looking forward to the concert', 'negative'),
                  ('He is my enemy', 'negative')]


    classifier = DictionarySentimentClassifier()

    long_text = """What can I say about this place. The staff of the restaurant is nice and the eggplant is not bad. Apart from that, very uninspired food, lack of atmosphere and too expensive. I am a staunch vegetarian and was sorely dissapointed with the veggie options on the menu. Will be the last time I visit, I recommend others to avoid."""
    print classifier.sentiment_of(long_text)

    #for text in pos_tweets + neg_tweets:
    #    print sentiment(text[0])




if __name__ == '__main__':
    demo()