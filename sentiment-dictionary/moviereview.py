import glob
import re

import learn


POSITIVE = 'positive'
NEGATIVE = 'negative'

def clean(sentence):
    return ' '.join(re.split('\W+', sentence))


def read_lines(file_name):
    with open(file_name, 'r') as file:
        return file.readlines()


def prepare_data(sentiment, filename):
    if sentiment == NEGATIVE:
        sentiments = ["0", "1"]
    else:
        sentiments = ["3", "4"]

    lines = read_lines(filename)
    return [(clean(line[2:]), sentiment) for line in lines if line[0] in sentiments]


def test_sentiment(sentiments, classifier):
    true_result = 0
    false_result = 0
    for sentence, sentiment in sentiments:
        if classifier.sentiment_of(sentence) == sentiment:
            true_result += 1
        else:
            false_result += 1

        print (true_result, false_result)

    return true_result, false_result

if __name__ == '__main__':
    test_file = '../sentiment-nltk/books.txt'

    pos_test = prepare_data(POSITIVE, test_file)
    neg_test = prepare_data(NEGATIVE, test_file)

    print len(pos_test)
    print len(neg_test)

    classifier = learn.DictionarySentimentClassifier()

    true_positive, false_positive = test_sentiment(pos_test, classifier)
    print('[INFO] positive examples tested')
    print('true positive: ', true_positive, 'missed positive: ', false_positive)

    

    true_negative, false_negative = test_sentiment(neg_test, classifier)
    print('[INFO] negative examples tested')
    print('true negative: ', true_negative, 'missed negative: ', false_negative)

    true_all = true_negative + true_positive
    false_all = false_negative + false_positive

    accuracy = float(true_all) / (true_all + false_all)

    print('true all: ', true_all, ' false all: ', false_all, ' accuracy: ', accuracy)
