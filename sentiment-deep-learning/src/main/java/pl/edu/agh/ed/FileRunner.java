package pl.edu.agh.ed;

import com.google.common.collect.ImmutableMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class FileRunner {
    private final static ImmutableMap<String, Boolean> sentimentStrings = ImmutableMap.<String, Boolean>builder()
            .put("Very negative", false)
            .put("Negative", false)
            .put("Neutral", false)
            .put("Positive", true)
            .put("Very positive", true)
            .build();

    private final static ImmutableMap<String, Boolean> sentimentValues = ImmutableMap.<String, Boolean>builder()
            .put("0", false)
            .put("1", false)
            .put("2", false)
            .put("3", true)
            .put("4", true)
            .build();

    private final static ImmutableMap<String, String> multiClassMapping = ImmutableMap.<String, String>builder()
            .put("Very negative", "0")
            .put("Negative", "1")
            .put("Neutral", "2")
            .put("Positive", "3")
            .put("Very positive", "4")
            .build();

    private static File getFile(String name) {
        return new File(FileRunner.class.getClassLoader().getResource(name).getFile());
    }

    private static final String DATA_DIRECTORY = "tweets/";

    public static void main(String[] args) throws IOException {
        // default tokenizer: \.|[!?]+

        BufferedReader br = new BufferedReader(new FileReader(getFile(DATA_DIRECTORY + "ranks.txt")));
        String[] sentiments = br.readLine().split(",");

        long startTime = System.currentTimeMillis();
        List<String> results = CustomSentimentPipeline.main(new String[]{"-file", DATA_DIRECTORY + "sentences.txt"}).get(0);
        long endTime = System.currentTimeMillis();

        System.out.println("Time: " + (endTime - startTime) + " ms");
        int size = Math.min(sentiments.length, results.size());

        System.out.println("SENTIMENTS len: " + sentiments.length);
        System.out.println("RESULTS len: " + results.size());
        int correct = 0;
        int fail = 0;
        int truePositive = 0;
        int falsePositive = 0;
        int trueNegative = 0;
        int falseNegative = 0;
        Map<String, AtomicInteger> multiClassSuccess = getMultiClassEmptyMap();
        Map<String, AtomicInteger> multiClassFailure = getMultiClassEmptyMap();
        int multiClassSuccessCount = 0;
        int multiClassFailureCount = 0;

        for (int i = 0; i < size; i++) {
            String result = results.get(i);
            String realSentimentValue = sentiments[i];
            if (sentimentStrings.get(result) == sentimentValues.get(realSentimentValue)) {
                correct++;
                if (sentimentStrings.get(result)) {
                    truePositive++;
                } else {
                    trueNegative++;
                }
            } else {
                fail++;
                if (sentimentStrings.get(result)) {
                    falseNegative++;
                } else {
                    falsePositive++;
                }
            }
            if (multiClassMapping.get(result).equals(realSentimentValue)) {
                multiClassSuccess.get(realSentimentValue).incrementAndGet();
                multiClassSuccessCount++;
            } else {
                multiClassFailure.get(realSentimentValue).incrementAndGet();
                multiClassFailureCount++;
            }
        }


        System.out.println("CORRECT: " + correct);
        System.out.println("FAIL: " + fail);
        System.out.println("TRUE POSITIVE: " + truePositive);
        System.out.println("FALSE POSITIVE: " + falsePositive);
        System.out.println("TRUE NEGATIVE: " + trueNegative);
        System.out.println("FALSE NEGATIVE: " + falseNegative);
        System.out.println("------------ MULTI CLASS");
        System.out.println("MLT-CLASS SUCCESS: " + multiClassSuccessCount);
        System.out.println("MLT-CLASS FAILURE: " + multiClassFailureCount);
        for (String sentimentValue : sentimentValues.keySet()) {
            System.out.println(sentimentValue + " SUCCESS: " + multiClassSuccess.get(sentimentValue).get());
            System.out.println(sentimentValue + " FAILURE: " + multiClassFailure.get(sentimentValue).get());
        }
    }

    private static Map<String, AtomicInteger> getMultiClassEmptyMap() {
        Map<String, AtomicInteger> multiClassMap = new HashMap<String, AtomicInteger>();
        multiClassMap.put("0", new AtomicInteger(0));
        multiClassMap.put("1", new AtomicInteger(0));
        multiClassMap.put("2", new AtomicInteger(0));
        multiClassMap.put("3", new AtomicInteger(0));
        multiClassMap.put("4", new AtomicInteger(0));
        return multiClassMap;
    }
}
