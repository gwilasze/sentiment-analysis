package pl.edu.agh.ed;

import com.google.common.collect.ImmutableMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Joiner.on;

public class FileListRunner {


    private final static ImmutableMap<String, Integer> sentimentValues = ImmutableMap.<String, Integer>builder()
            .put("Very negative", 0)
            .put("Negative", 1)
            .put("Neutral", 2)
            .put("Positive", 3)
            .put("Very positive", 4)
            .build();

    private static String getFilesNames() {
        List<String> filesNames = new LinkedList<String>();
        for (int i = 0; i < 2000; i++) {
            filesNames.add("books/review" + i + ".txt");
        }
        return on(",").join(filesNames);
    }

    private static String[] getRanks() {
        try {
            File file = new File(FileListRunner.class.getClassLoader().getResource("books/ranks.txt").getFile());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String[] sentiments = br.readLine().split(",");
            br.close();
            return sentiments;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return 0, 1, 3 or 4
     */
    private static String getOverallSentiment(double averageSentiment) {
        if (averageSentiment > 3.0) {
            return "4";
        } else if (averageSentiment > 2.0) {
            return "3";
        } else if (averageSentiment >= 1.0) {
            return "1";
        } else {
            return "0";
        }
    }

    private static boolean isPositive(String rank) {
        switch (rank) {
            case "3":
            case "4":
                return true;
            default:
                return false;
        }
    }

    public static void main(String[] args) throws IOException {
        String filesNames = getFilesNames();
        String[] ranks = getRanks();

        long startTime = System.currentTimeMillis();
        List<List<String>> results = CustomSentimentPipeline.main(new String[]{"-fileList", filesNames});
        long endTime = System.currentTimeMillis();

        System.out.println("Time: " + (endTime - startTime) + " ms");

        System.out.println("SENTIMENTS len: " + ranks.length);
        System.out.println("RESULTS len: " + results.size());


        int positiveSuccess = 0;
        int positiveFail = 0;
        int negativeSuccess = 0;
        int negativeFail = 0;
        int success = 0;
        int fail = 0;

        Map<String, AtomicInteger> multiClassSuccess = getMultiClassEmptyMap();
        Map<String, AtomicInteger> multiClassFail = getMultiClassEmptyMap();
        int multiClassSuccessCount = 0;
        int multiClassFailureCount = 0;

        for (int i = 0; i < Math.min(results.size(), ranks.length); i++) {

            List<String> result = results.get(i);
            String realRank = ranks[i];

            double sumOfSentiments = 0;
            for (String sentiment : result) {
                sumOfSentiments += sentimentValues.get(sentiment);
            }

            double averageSentiment = sumOfSentiments / result.size();
            String overallSentiment = getOverallSentiment(averageSentiment);

            if (realRank.equals(overallSentiment)) {
                multiClassSuccess.get(realRank).incrementAndGet();
                multiClassSuccessCount++;
            } else {
                multiClassFail.get(realRank).incrementAndGet();
                multiClassFailureCount++;
            }

            if (isPositive(realRank)) {
                if (isPositive(overallSentiment)) {
                    positiveSuccess++;
                    success++;
                } else {
                    positiveFail++;
                    fail++;
                }
            } else {
                if (isPositive(overallSentiment)) {
                    negativeFail++;
                    fail++;
                } else {
                    negativeSuccess++;
                    success++;
                }
            }

        }

        System.out.println("CORRECT: " + success);
        System.out.println("FAIL: " + fail);
        System.out.println("success POSITIVE: " + positiveSuccess);
        System.out.println("fail POSITIVE: " + positiveFail);
        System.out.println("success NEGATIVE: " + negativeSuccess);
        System.out.println("fail NEGATIVE: " + negativeFail);
        System.out.println("------------ MULTI CLASS");
        System.out.println("MLT-CLASS SUCCESS: " + multiClassSuccessCount);
        System.out.println("MLT-CLASS FAILURE: " + multiClassFailureCount);
        for (String sentimentValue : multiClassSuccess.keySet()) {
            System.out.println(sentimentValue + " SUCCESS: " + multiClassSuccess.get(sentimentValue).get());
            System.out.println(sentimentValue + " FAILURE: " + multiClassFail.get(sentimentValue).get());
        }
    }

    private static Map<String, AtomicInteger> getMultiClassEmptyMap() {
        Map<String, AtomicInteger> multiClassMap = new HashMap<String, AtomicInteger>();
        multiClassMap.put("0", new AtomicInteger(0));
        multiClassMap.put("1", new AtomicInteger(0));
        multiClassMap.put("3", new AtomicInteger(0));
        multiClassMap.put("4", new AtomicInteger(0));
        return multiClassMap;
    }
}
