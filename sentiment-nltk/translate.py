import re
from string import punctuation


def clear_text(sentence):
    for pattern in ["n't", "'", ",", ".", "'", "?", "!"]:
        sentence = sentence.replace(" " + pattern, pattern)

    if sentence.endswith("."):
        return sentence
    else:
        return sentence + "."


def prepare(sentence):
    sentiment = sentence[1]
    return clear_text(
        ' '.join([word for word in re.split("[\s\)]+|\([0-4]", sentence) if word != ""])), sentiment


def read_lines(file_name):
    with open(file_name, 'r', encoding='utf-8') as file:
        return file.readlines()


def prepare_data(file_name):
    return [sentence for sentence in [prepare(line) for line in read_lines(file_name)] if sentence is not None]


f = open("test_formatted.txt", 'w', encoding='utf-8')

data = prepare_data("test.txt")

for sentence, sentiment in data:
    f.write(sentiment + "," + sentence + "\n")

f.close()

