import glob
import re

import learn


POSITIVE = 'positive'
NEGATIVE = 'negative'


def clean(sentence):
    return ' '.join(re.split('\W+', sentence))


def file_names(pattern):
    return glob.glob(pattern)


def read_content(file_name):
    with open(file_name, 'r', encoding='utf-8') as file:
        return file.read()


def prepare_data(sentiment, file_pattern):
    return [(clean(read_content(file_name)), sentiment) for file_name in file_names(file_pattern)]


def test_sentiment(sentiments, classifier):
    true_result = 0
    false_result = 0
    for sentence, sentiment in sentiments:
        if classifier.classify(sentence) == sentiment:
            true_result += 1
        else:
            false_result += 1

    return true_result, false_result

if __name__ == '__main__':
    positive_dir = 'C:/Users/g.wilaszek/Desktop/txt_sentoken/neg/*.txt'
    negative_dir = 'C:/Users/g.wilaszek/Desktop/txt_sentoken/pos/*.txt'
    pos_test_dir = 'C:/Users/g.wilaszek/Desktop/aclImdb/test/pos/2*.txt'
    neg_test_dir = 'C:/Users/g.wilaszek/Desktop/aclImdb/test/neg/2*.txt'

    pos_reviews = prepare_data(POSITIVE, positive_dir)
    neg_reviews = prepare_data(NEGATIVE, negative_dir)
    classifier = learn.build_classifier(pos_reviews, neg_reviews)
    print('[INFO] classifier built successfully')

    pos_test = prepare_data(POSITIVE, pos_test_dir)

    true_positive, false_positive = test_sentiment(pos_test, classifier)
    print('[INFO] positive examples tested')
    print('true positive: ', true_positive, 'false positive: ', false_positive)

    neg_test = prepare_data(NEGATIVE, neg_test_dir)

    true_negative, false_negative = test_sentiment(neg_test, classifier)
    print('[INFO] negative examples tested')
    print('true negative: ', true_negative, 'false negative: ', false_negative)
