import glob
import re
import time

import learn


POSITIVE = 'positive'
NEGATIVE = 'negative'

sentiments_map = {
    "0": NEGATIVE,
    "1": NEGATIVE,
    "2": NEGATIVE,
    "3": POSITIVE,
    "4": POSITIVE
}


def prepare(sentence):
    sentiment = sentiments_map[sentence[1]]
    return ' '.join([word for word in re.split('\W+|[0-4]', sentence) if word != ""]), sentiment


def file_names(pattern):
    return glob.glob(pattern)


def read_lines(file_name):
    with open(file_name, 'r', encoding='utf-8') as file:
        return file.readlines()


def prepare_data(file_name):
    return [sentence for sentence in [prepare(line) for line in read_lines(file_name)] if sentence is not None]


def prepare_formatted(sentence):
    return sentence[2:], (sentiments_map[sentence[0]])


def prepare_formatted_data(file_name):
    return [sentence for sentence in [prepare_formatted(line) for line in read_lines(file_name)] if sentence is not None]


def test_sentiment(sentiments, classifier):
    true_result = 0
    false_result = 0
    for sentence, sentiment in sentiments:
        if classifier.classify(sentence) == sentiment:
            true_result += 1
        else:
            false_result += 1

    return true_result, false_result


if __name__ == '__main__':
    train_file = 'train.txt'
    test_file = 'books.txt'

    reviews = prepare_data(train_file)

    learning_start = time.time()
    classifier = learn.build_classifier(reviews, [])
    learning_end = time.time()
    print('Learning time: ' + str(learning_end - learning_start) + ' s')

    test_data = prepare_formatted_data(test_file)
    pos_test = [t for t in test_data if t[1] == POSITIVE]
    neg_test = [t for t in test_data if t[1] == NEGATIVE]

    start = time.time()
    true_positive, false_positive = test_sentiment(pos_test, classifier)
    true_negative, false_negative = test_sentiment(neg_test, classifier)
    end = time.time()

    print('Classification time: ' + str(end - start) + ' s')
    print('true positive: ', true_positive, 'false positive: ', false_positive)

    print('true negative: ', true_negative, 'false negative: ', false_negative)
