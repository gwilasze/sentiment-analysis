import re
from unittest.test.testmock.testpatch import function

import nltk


class Classifier(object):
    def __init__(self, bayes_classifier: nltk.NaiveBayesClassifier, extract_function: function):
        self._bayes_classifier = bayes_classifier
        self._extract_function = extract_function

    def classify(self, sentence):
        words = re.split('\W+', sentence)
        words.remove('')
        return self._bayes_classifier.classify(self._extract_function(words))


def get_words_in_tweets(tweets):
    all_words = []
    for (words, sentiment) in tweets:
        all_words.extend(words)
    return all_words


def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features


def extract_features_function(word_features):
    def extract_function(document):
        document_words = set(document)
        features = {}
        for word in word_features:
            features['contains(%s)' % word] = (word in document_words)
        return features

    return extract_function


def build_classifier(positive, negative):
    tweets = []
    for (words, sentiment) in positive + negative:
        words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
        tweets.append((words_filtered, sentiment))

    word_features = get_word_features(get_words_in_tweets(tweets))

    extract_features = extract_features_function(word_features)
    training_set = nltk.classify.apply_features(extract_features, tweets)
    classifier = nltk.NaiveBayesClassifier.train(training_set)

    return Classifier(classifier, extract_features)


def demo():
    pos_tweets = [('I love this car', 'positive'),
                  ('This view is amazing', 'positive'),
                  ('I feel great this morning', 'positive'),
                  ('I am so excited about the concert', 'positive'),
                  ('He is my best friend', 'positive')]

    neg_tweets = [('I do not like this car', 'negative'),
                  ('This view is horrible', 'negative'),
                  ('I feel tired this morning', 'negative'),
                  ('I am not looking forward to the concert', 'negative'),
                  ('He is my enemy', 'negative')]

    cl = build_classifier(pos_tweets, neg_tweets)
    print(cl.classify('I fill happy this morning'))


if __name__ == '__main__':
    demo()